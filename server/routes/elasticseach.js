const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';


var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});


/* GET api listing. */
router.get('/', (req, res) => {
 // res.send('api works');

  client.ping({
  // ping usually has a 3000ms timeout
  requestTimeout: 1000
}, function (error) {
  if (error) {
    console.trace('elasticsearch cluster is down!');
  } else {
    console.log('All is well');
      res.send('api works');
  }
});


});
 


router.get('/all', (req, res) => {
    client.search({
      index: 'restaurants', 
      type: 'restaurant',
      body: {
        query: {
              bool: {
                  must: [
                    { match_all: {

                    }}
                  ],
                  filter: {
                    geo_distance : {
                      distance : "1km",
                      locations : {
                      lat : 16.046381,
                      lon : 108.218250
                      }
                    }
                  }
                }
              }
      }
  }).then(function (body) {
      var hits = body.hits.hits;
      console.log('hits : ',hits);
       res.status(200).json(hits);
  }, function (error) {
    console.trace(error.message);
  });


})

// Get all posts
router.get('/posts', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  axios.get(`${API}/posts`)
    .then(posts => {
      res.status(200).json(posts.data);
    })
    .catch(error => {
      res.status(500).send(error)
    });
});

module.exports = router;
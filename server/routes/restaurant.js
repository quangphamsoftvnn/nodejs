const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');
const API = 'http://localhost:9200';
 
/* GET api listing. */
router.get('/', (req, res) => {
  res.send(res);
});
 
 /* get all */
 router.get('/restaurant',(req,res)=>{
    axios.get(`${API}/restaurants/_search`).then(restaurans => {
        console.log('restaurans.data : ',restaurans.data);
        res.status(200).json(restaurans.data);
    }).catch(error => {
        res.status(500).send(error);
    })
 });

 router.get('/search',(req,res)=>{
     var q = req.query.q;
    axios.get(`${API}/restaurants/_search?q=`+q).then(restaurans => {
        res.status(200).json(restaurans.data);
    }).catch(error => {
        res.status(500).send(error);
    })
 });

/*get by id */

 router.get('/restaurant/:id',(req,res)=>{
    var id = req.params.id;
    axios.get(`${API}/restaurants/restaurant/${id}`).then(restaurans => {
        res.status(200).json(restaurans.data);
    }).catch(error => {
        res.status(500).send(error);
    })
 });


module.exports = router;
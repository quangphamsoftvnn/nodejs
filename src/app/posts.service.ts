import { Injectable } from '@angular/core';
import  { Http } from '@angular/http';
import  'rxjs/add/operator/map';
import { UtilsServiceService } from './utils/utils-service.service';
@Injectable()
export class PostsService {

  constructor ( private http : Http,
    private utilsService : UtilsServiceService
  ) {


   }

   getAllPosts() {
     return this.utilsService.get('api/posts'); 
   }

}

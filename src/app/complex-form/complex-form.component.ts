import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidatorService } from '../utils/validator.service'
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-complex-form',
  templateUrl: './complex-form.component.html',
  styleUrls: ['./complex-form.component.css']
})
export class ComplexFormComponent implements OnInit {
  complexForm: FormGroup;

  constructor(fb: FormBuilder) {
    this.complexForm = fb.group({
      'firstName': [null, Validators.required],
      'email': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100), ValidatorService.checkEmailFormat])],
      'gender': [null, Validators.required],
      'lastName': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])],
      'hiking': [false],
      'running': [false],
      'swimming': [false]
    });
    console.log(this.complexForm);
    this.complexForm.valueChanges.subscribe((form: any) => {
     // console.log('form changed to:', form);
    });

  }

  ngOnInit() {
  }
  submitForm(value: any) {
    console.log('Reactive Form Data: ')
    console.log(value);
  }
}

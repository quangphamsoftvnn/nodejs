import { Injectable } from '@angular/core';
import  { Http } from '@angular/http';
import  'rxjs/add/operator/map';

@Injectable()
export class UtilsServiceService { 
   hostURl : string = "http://localhost:3000/";

   constructor( private http : Http) { 
   } 
   get(url: string): any { 

      return this.http.get(this.hostURl + url).map(res => res.json());
   }

}

import { TestBed, inject } from '@angular/core/testing';

import { UtilsServiceService } from './utils-service.service';
import {Response, ResponseOptions, Http, HttpModule, ConnectionBackend, BaseRequestOptions, RequestOptions} from '@angular/http';
describe('UtilsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConnectionBackend,UtilsServiceService, Http],
        imports: [HttpModule ],
    });
  });

  it('should ...', inject([UtilsServiceService], (service: UtilsServiceService) => {
    expect(service).toBeTruthy();
  }));
});

interface Scripts {
    name: string;
    src: string;
}
export const ScriptStore: Scripts[] = [
    { name: 'modernizr', src: 'assets/js/vendor/modernizr-2.6.2.min.js' },
    { name: 'jquery', src: 'assets/js/vendor/jquery-1.10.2.min.js' },
    { name: 'jquery.nav', src: 'assets/js/jquery.nav.js' },
    { name: 'jquery.sticky', src: 'assets/js/jquery.sticky.js' },
    { name: 'plugins', src: 'assets/js/plugins.js' },
    { name: 'wow.min', src: 'assets/js/wow.min.js' },
    { name: 'main', src: 'assets/js/main.js' },
]; 
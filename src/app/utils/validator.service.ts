import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

interface ValidationResult {
  [key: string]: boolean;
}


@Injectable()
export class ValidatorService {

  constructor() { }
  static checkEmailFormat(control: AbstractControl): ValidationResult {
    console.log('email format mailFormatmailFormat');
    var EMAIL_REGEXP = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    if (!EMAIL_REGEXP.test(control.value)) {
      return { "incorrectMailFormat": true };
    }
    return null;
  }
  public static simple(control: AbstractControl): ValidationResult {
    console.log('email format simple');
    //  if (Util.isNotPresent(control)) return undefined;
    let pattern = '^.+@.+\\..+$';
    if (new RegExp(pattern).test(control.value)) {
      return null;
    }
    return { 'simpleEmailRule': true };
  }

  public static normal(control: AbstractControl): ValidationResult {
    console.log('email format normal ValidationResult');
    //if (Util.isNotPresent(control)) return undefined;
    // tslint:disable-next-line:max-line-length
    let pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if (pattern.test(control.value)) {
      return null;
    }
    return { 'normalEmailRule': true };
  }
}

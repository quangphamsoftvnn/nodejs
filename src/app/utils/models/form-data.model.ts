import { Question } from "app/utils/models/question.model";

export interface FormData {
    id: number;
    questions : Array<Question>;
    title : string;
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DymanicLoadComponentComponent } from './dymanic-load-component.component';

describe('DymanicLoadComponentComponent', () => {
  let component: DymanicLoadComponentComponent;
  let fixture: ComponentFixture<DymanicLoadComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DymanicLoadComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DymanicLoadComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

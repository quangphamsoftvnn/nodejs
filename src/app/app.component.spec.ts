import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { PostsComponent } from './posts/posts.component';

import { PostsService } from './posts.service';
import { RestaurantComponent } from './restaurant/restaurant.component'
import { RestaurantService } from './restaurant/restaurant.service';
import { AnimationsComponent } from './animations/animations.component'

import { UtilsServiceService } from './utils/utils-service.service';
import { RetaurantDetailComponent } from './retaurant-detail/retaurant-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { ScriptService } from './utils/script.service';
import { RegisterComponent } from './register/register.component';
import { ComplexFormComponent } from './complex-form/complex-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  RouterTestingModule
} from '@angular/router/testing';

const ROUTES = [
  {
    path: '',
    redirectTo: 'restaurant',
    pathMatch: 'full'
  },
  {
    path: 'posts',
    component: PostsComponent
  }
];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PostsComponent,
        RestaurantComponent,
        AnimationsComponent,
        RetaurantDetailComponent,
        PageNotFoundComponent,
        RegisterComponent,
        ComplexFormComponent,
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(ROUTES)
      ],
       providers: [UtilsServiceService,PostsService, RestaurantService, ScriptService],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app works!');
  }));
});

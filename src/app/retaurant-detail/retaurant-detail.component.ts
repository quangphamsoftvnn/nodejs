import { Component, OnInit } from '@angular/core';
import { ScriptService } from '../utils/script.service'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RestaurantService } from '../restaurant/restaurant.service'

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-retaurant-detail',
  templateUrl: './retaurant-detail.component.html',
  styleUrls: ['./retaurant-detail.component.css',
    '../../assets/css/owl.carousel.css',
    '../../assets/css/bootstrap.min.css', 
    '../../assets/css/font-awesome.min.css',
    '../../assets/css/animate.min.css',
    '../../assets/css/main.css',
    '../../assets/css/responsive.css'
  ]
})

export class RetaurantDetailComponent implements OnInit {

  restaurant: any;
  constructor(
    private script: ScriptService,
    private route: ActivatedRoute,
    private router: Router,
    private restaurantService: RestaurantService

  ) {
    this.script.load('modernizr',  'jquery.nav', 'jquery.sticky', 'plugins', 'wow.min', 'main').then(data => {
      console.log('script is load successful');
    }).catch(error => {
      console.log('Error : ', error);
    });
  }

  ngOnInit() { 
    this.route.params.switchMap((params: Params) => this.restaurantService.getById(params['id']))
      .subscribe((restaurant: any) => this.restaurant = restaurant._source);
  }


}

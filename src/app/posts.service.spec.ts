import { TestBed, inject } from '@angular/core/testing';

import { PostsService } from './posts.service'; 
import {MockBackend, MockConnection} from '@angular/http/testing';
import {Response, ResponseOptions, Http, HttpModule, ConnectionBackend, BaseRequestOptions, RequestOptions} from '@angular/http';
import { UtilsServiceService } from './utils/utils-service.service';
describe('PostsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConnectionBackend, PostsService, Http,UtilsServiceService],
      imports: [HttpModule ], 
    });
  });

  it('should ...', inject([PostsService], (service: PostsService) => {
    expect(service).toBeTruthy();
  }));
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';

import { PostsService } from './posts.service';
import { RestaurantComponent } from './restaurant/restaurant.component'
import { RestaurantService } from './restaurant/restaurant.service';
import { AnimationsComponent } from './animations/animations.component'

import { UtilsServiceService } from './utils/utils-service.service';
import { RetaurantDetailComponent } from './retaurant-detail/retaurant-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { ScriptService } from './utils/script.service';
import { RegisterComponent } from './register/register.component';
// import { ComplexFormComponent } from './complex-form/complex-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DymanicLoadComponentComponent } from './dymanic-load-component/dymanic-load-component.component';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';



const ROUTES = [
  {
    path: '',
    redirectTo: 'restaurant',
    pathMatch: 'full'
  },

  {
    path: "restaurant",
    component: RestaurantComponent
  },
  {
    path: "animations",
    component: AnimationsComponent
  },
  {
    path: "restaurant/details/:id",
    component: RetaurantDetailComponent
  },
  {
    path: 'posts',
    component: PostsComponent
  }, {
    path: 'register',
    component: RegisterComponent
  }, 
  {
    path: 'employees',
    component: EmployeesComponent
  },
  {
    path: 'edit/:id',
    component: EmployeeEditComponent
  },
  {
    path: 'add',
    component: EmployeeAddComponent
  },
  // {
  //   path: 'complex-form',
  //   component: ComplexFormComponent
  // },
  // {
  //   path: 'dynamic-load-component',
  //   component: DymanicLoadComponentComponent
  // },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    RestaurantComponent,
    AnimationsComponent,
    RetaurantDetailComponent,
    PageNotFoundComponent,
    RegisterComponent,
    EmployeesComponent,
    EmployeeEditComponent,
    EmployeeAddComponent,
    // ComplexFormComponent,
    // DymanicLoadComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [UtilsServiceService, PostsService, RestaurantService, ScriptService],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule {


  componentData = null;

  createHelloWorldComponent() {
    //   this.componentData = {
    //     component: HelloWorldComponent,
    //     inputs: {
    //       showNum: 9
    //     }
    //   };
    // }

    // createWorldHelloComponent() {
    //   this.componentData = {
    //     component: WorldHelloComponent,
    //     inputs: {
    //       showNum: 2
    //     }
    //   };
    // }
  }

}

import { Injectable } from '@angular/core';
import  { Http } from '@angular/http';
import  'rxjs/add/operator/map';
import { UtilsServiceService } from '../utils/utils-service.service';

@Injectable()
export class RestaurantService {

   constructor ( private http : Http,
   private utilsService :UtilsServiceService 
   ) {


   }

   getAllRestaurant() : any{
       return this.utilsService.get('restaurants/restaurant');
   }
   searhRestaurant(searchValue : string) : any {
     return this.http.get('restaurants/search?q='+searchValue).map(res=>res.json());
   }

   getById(id : string){
     return this.utilsService.get('restaurants/restaurant/'+ id);
   }

}

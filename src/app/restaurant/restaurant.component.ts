import { Component, OnInit } from '@angular/core';
import { RestaurantService } from './restaurant.service'

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})

export class RestaurantComponent implements OnInit {

  restaurants : any = [];
  searchValue : string = "";
  constructor( private restaurantService : RestaurantService) { }

  ngOnInit() {
      this.restaurantService.getAllRestaurant().subscribe(data=> {
        this.restaurants = data.hits;
      });

  }

  searchRestaurant() : void {

    this.restaurantService.searhRestaurant(this.searchValue).subscribe(data=> {
          this.restaurants = data.hits;
        });
  }


}
